#!/bin/sh
## install python requirements
#pip install -r requirements.txt

## install nikola orgmode addon
nikola plugin -i orgmode

## install emacs
EVM_EMACS=emacs-25.3-travis
# only workes for Travis :
#curl -fsSkL https://gist.github.com/rejeep/ebcd57c3af83b049833b/raw > x.sh && source ./x.sh
#evm install $EVM_EMACS --use --skip

# setup Evm (Emacs Version Manager) and Cask on netlify.com :
export PATH="/opt/buildhome/.evm/bin:$PATH"
export PATH="/opt/buildhome/.cask/bin:$PATH"

git clone https://github.com/rejeep/evm.git /opt/buildhome/.evm
evm config path /tmp
evm install $EVM_EMACS --use --skip
curl -fsSkL https://raw.github.com/cask/cask/master/go | python

## build
nikola build